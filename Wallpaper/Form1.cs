﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
// using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Wallpaper
{
    public partial class Form1 : Form
    {
            [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            public static extern int SystemParametersInfo(int uAction, int uParam, IntPtr lpvParam, int fuWinIni);
          
            public const int SPI_SETDESKWALLPAPER = 20;
            public const int SPIF_UPDATEINIFILE = 0x1;
            public const int SPIF_SENDWININICHANGE = 0x2;
            public int CurrentWallpepers;
            public int CurrentDayState;
            public bool change;
            //  Random rand1 = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            public Random rand1 = new Random();
       

        public Form1()
        {
            InitializeComponent();

        }


        private void Form1_Load(object sender, EventArgs e)
        {
            notifyIcon1.Visible = true;
            this.Show();

            String time = System.IO.File.ReadAllText(".\\Settings\\Time.txt");
            String[] splitday = time.Split(',');
            String[] splittime = splitday[0].Split(':');

            HourOfMorning.Value = Convert.ToInt32(splittime[0]);
            MinuteOfMorning.Value = Convert.ToInt32(splittime[1]);
            
            splittime = splitday[1].Split(':');
            HourOfDay.Value = Convert.ToInt32(splittime[0]);
            MinuteOfDay.Value = Convert.ToInt32(splittime[1]);

            splittime = splitday[2].Split(':');
            HourOfEvening.Value = Convert.ToInt32(splittime[0]);
            MinuteOfEvening.Value = Convert.ToInt32(splittime[1]);

            splittime = splitday[3].Split(':');
            HourOfNight.Value = Convert.ToInt32(splittime[0]);
            MinuteOfNight.Value = Convert.ToInt32(splittime[1]);


            MorningWallpapersRich.LoadFile(".\\Settings\\Morning.txt", RichTextBoxStreamType.PlainText);
            DayWallpapersRich.LoadFile(".\\Settings\\Day.txt", RichTextBoxStreamType.PlainText);
            EveningWallpapersRich.LoadFile(".\\Settings\\Evening.txt", RichTextBoxStreamType.PlainText);
            NightWallpapersRich.LoadFile(".\\Settings\\Night.txt", RichTextBoxStreamType.PlainText);

            String Set = System.IO.File.ReadAllText(".\\Settings\\Settings.txt");
            String[] splitoptions = Set.Split(',');

            if (splitoptions[0] == "0") CheckRandom.Checked = false;
            else CheckRandom.Checked = true;
            if (splitoptions[1] == "0") CheckInterval.Checked = false;
            else CheckInterval.Checked = true;
            if (splitoptions[2] == "0") CheckStartChange.Checked = false;
            else CheckStartChange.Checked = true;

            IntervalOfMorning.Value = Convert.ToInt32(splitoptions[3]);
            IntervalOfDay.Value     = Convert.ToInt32(splitoptions[4]);
            IntervalOfEvening.Value = Convert.ToInt32(splitoptions[5]);
            IntervalOfNight.Value   = Convert.ToInt32(splitoptions[6]);

            CurrentDayState = state();
            CurrentWallpepers = -1;
            change = true;

            if (CheckStartChange.Checked)
            {
                if (MorningWallpapersRich.Text == "" || DayWallpapersRich.Text == "" || EveningWallpapersRich.Text == "" || NightWallpapersRich.Text == "")
                { MessageBox.Show(@"One or more list of wallpapers is empty !!!"); timer1.Enabled = false; timer2.Enabled = false; button6.Enabled = false; button1.Enabled = true; }
                else
                {
                    button1.Enabled = false;
                    ChageWallpeper(); change = false;
                    timer1.Enabled = true;
                    this.WindowState = FormWindowState.Minimized;
                    this.Hide();
                    this.ShowInTaskbar = false;
                }
            }
            else button6.Enabled = false;

        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Hide(); this.ShowInTaskbar = false;
            }
        }

        private void Show_Click(object sender, EventArgs e)
        {
            this.Show();
            this.ShowInTaskbar = true;
            this.WindowState = FormWindowState.Normal;
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
         ChageWallpeper();
        }


        private void button3_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                for (int i = 0; i < openFileDialog1.FileNames.Length; i++)
                {
                    if (MorningWallpapersRich.Lines.Length != 0)
                        MorningWallpapersRich.AppendText("\n");
                    MorningWallpapersRich.AppendText(openFileDialog1.FileNames[i]);
                }
            }
        }


        private void timer2_Tick(object sender, EventArgs e)
        {
 
            if (CurrentDayState != state())
            { CurrentWallpepers = -1; CurrentDayState = state(); change = false; }

            ChageWallpeper();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MorningWallpapersRich.Text == "" || DayWallpapersRich.Text == "" || EveningWallpapersRich.Text == "" || NightWallpapersRich.Text == "")
            { MessageBox.Show(@"One or more list of wallpapers is empty !!!"); timer1.Enabled = false; timer2.Enabled = false; button6.Enabled = false; button1.Enabled = true; }
            else
            {
                button1.Enabled = false;
                button6.Enabled = true;
                timer1.Enabled = true;
                ChageWallpeper();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            if (MorningWallpapersRich.Text == "" || DayWallpapersRich.Text == "" || EveningWallpapersRich.Text == "" || NightWallpapersRich.Text == "")
            { MessageBox.Show(@"One or more list of wallpapers is empty !!!"); timer1.Enabled = false; timer2.Enabled = false; button6.Enabled = false; button1.Enabled = true; return; }
            else button1.Enabled = false; 

            if (CurrentDayState != state())
            { CurrentDayState = state(); change = true; CurrentWallpepers = -1; }

            if (CheckInterval.Checked)
            {
                if (CurrentDayState == 1) timer2.Interval = ((int)IntervalOfMorning.Value) * 60 * 1000;
                if (CurrentDayState == 2) timer2.Interval = ((int)IntervalOfDay.Value) * 60 * 1000;
                if (CurrentDayState == 3) timer2.Interval = ((int)IntervalOfEvening.Value) * 60 * 1000;
                if (CurrentDayState == 4) timer2.Interval = ((int)IntervalOfNight.Value) * 60 * 1000;  
                
                   // timer2.Interval = ((int)IntervalOfMorning.Value) * 1000;
                    timer2.Enabled = true;
            }
            
            if(change)
            { 
                ChageWallpeper();    
                change = false;
            }
           
        }

        // Функция для определения текущего времени дня
        public int state()
        {
            
            int i = 0;
            String m, d, e, n;

            m = HourOfMorning.Value + ":" + MinuteOfMorning.Value;
            d = HourOfDay.Value + ":" + MinuteOfDay.Value;
            e = HourOfEvening.Value + ":" + MinuteOfEvening.Value;
            n = HourOfNight.Value + ":" + MinuteOfNight.Value;

            if (DateTime.Now.Ticks < Convert.ToDateTime(n).Ticks)
            { i = 3; }
            if (DateTime.Now.Ticks < Convert.ToDateTime(e).Ticks)
            { i = 2; }
            if (DateTime.Now.Ticks < Convert.ToDateTime(d).Ticks)
            { i = 1; }
            if (DateTime.Now.Ticks < Convert.ToDateTime(m).Ticks) 
            { i = 4; }
            if (DateTime.Now.Ticks > Convert.ToDateTime(n).Ticks && DateTime.Now.Ticks > Convert.ToDateTime(m).Ticks && i==0)
            { i = 4; }


            return i;
        }

        // Функция смена обоев
        public void ChageWallpeper()
        {
            String path="";

            if (CheckRandom.Checked)
            {
                if (CurrentDayState == 1)
                {
                    int i = rand1.Next(MorningWallpapersRich.Lines.Length);
                    path = MorningWallpapersRich.Lines[i].ToString(); 
                }
                if (CurrentDayState == 2)
                {
                    int i = rand1.Next(DayWallpapersRich.Lines.Length);
                    path = DayWallpapersRich.Lines[i].ToString();
                }
                if (CurrentDayState == 3)
                {
                    int i = rand1.Next(EveningWallpapersRich.Lines.Length);
                    path = EveningWallpapersRich.Lines[i].ToString();
                }
                if (CurrentDayState == 4)
                {
                    int i = rand1.Next(NightWallpapersRich.Lines.Length);
                    path = NightWallpapersRich.Lines[i].ToString();
                }
            }
            else
            {
                if (!CheckInterval.Checked)
                {
                   if (CurrentDayState == 1) path = MorningWallpapersRich.Lines[0].ToString();
                   if (CurrentDayState == 2) path = DayWallpapersRich.Lines[0].ToString();
                   if (CurrentDayState == 3) path = EveningWallpapersRich.Lines[0].ToString();
                   if (CurrentDayState == 4) path = NightWallpapersRich.Lines[0].ToString();
                }
                else
                {

                    if (CurrentWallpepers <= MorningWallpapersRich.Lines.Length && CurrentDayState == 1)
                    { CurrentWallpepers++; path = MorningWallpapersRich.Lines[CurrentWallpepers].ToString();
                    if (CurrentWallpepers + 1 >= MorningWallpapersRich.Lines.Length) CurrentWallpepers = -1;
                    }

                    if (CurrentWallpepers <= DayWallpapersRich.Lines.Length && CurrentDayState == 2)
                    { CurrentWallpepers++; path = DayWallpapersRich.Lines[CurrentWallpepers].ToString();
                    if (CurrentWallpepers + 1 >= DayWallpapersRich.Lines.Length) CurrentWallpepers = -1;
                    }
                    
                    if (CurrentWallpepers <= EveningWallpapersRich.Lines.Length && CurrentDayState == 3)
                    { CurrentWallpepers++; path = EveningWallpapersRich.Lines[CurrentWallpepers].ToString();
                    if (CurrentWallpepers + 1 >= EveningWallpapersRich.Lines.Length) CurrentWallpepers = -1;
                    }

                    if (CurrentWallpepers <= NightWallpapersRich.Lines.Length && CurrentDayState == 4)
                    { CurrentWallpepers++; path = NightWallpapersRich.Lines[CurrentWallpepers].ToString();
                    if (CurrentWallpepers + 1 >= NightWallpapersRich.Lines.Length) CurrentWallpepers = -1;
                    }

                }
            }

            SystemParametersInfo(SPI_SETDESKWALLPAPER, 1, Marshal.StringToBSTR(path), SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
            label6.Text = System.IO.Path.GetFileNameWithoutExtension(path);

            if (CurrentDayState == 1) { label24.Text = "Доброе утро!"; notifyIcon1.Text = "Доброе утро!"; }
            if (CurrentDayState == 2) { label24.Text = "Добрый день!"; notifyIcon1.Text = "Добрый день!"; }
            if (CurrentDayState == 3) { label24.Text = "Добрый вечер!"; notifyIcon1.Text = "Добрый вечер!"; }
            if (CurrentDayState == 4) { label24.Text = "Добрый ночи!"; notifyIcon1.Text = "Добрый ночи!"; }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            button6.Enabled = false; button1.Enabled = true;
            timer1.Enabled = false;
            timer2.Enabled = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                for (int i = 0; i < openFileDialog1.FileNames.Length; i++)
                {
                    if (DayWallpapersRich.Lines.Length != 0)
                        DayWallpapersRich.AppendText("\n");
                    DayWallpapersRich.AppendText(openFileDialog1.FileNames[i]);
                }
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.IO.File.WriteAllText(".\\Settings\\Time.txt", HourOfMorning.Value + ":" + MinuteOfMorning.Value + "," +
                             HourOfDay.Value + ":" + MinuteOfDay.Value + "," +
                             HourOfEvening.Value + ":" + MinuteOfEvening.Value + "," +
                             HourOfNight.Value + ":" + MinuteOfNight.Value
                 );

            MorningWallpapersRich.SaveFile(".\\Settings\\Morning.txt", RichTextBoxStreamType.PlainText);
            DayWallpapersRich.SaveFile(".\\Settings\\Day.txt", RichTextBoxStreamType.PlainText);
            EveningWallpapersRich.SaveFile(".\\Settings\\Evening.txt", RichTextBoxStreamType.PlainText);
            NightWallpapersRich.SaveFile(".\\Settings\\Night.txt", RichTextBoxStreamType.PlainText);

            String opt = "";
            if (CheckRandom.Checked) opt += "1,"; else opt += "0,";
            if (CheckInterval.Checked) opt += "1,"; else opt += "0,";
            if (CheckStartChange.Checked) opt += "1,"; else opt += "0,";
            opt += IntervalOfMorning.Value + ","; opt += IntervalOfDay.Value + ","; opt += IntervalOfEvening.Value + ","; opt += IntervalOfNight.Value + ",";
            System.IO.File.WriteAllText(".\\Settings\\Settings.txt", opt);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                for (int i = 0; i < openFileDialog1.FileNames.Length; i++)
                {
                    if (EveningWallpapersRich.Lines.Length != 0)
                        EveningWallpapersRich.AppendText("\n");
                    EveningWallpapersRich.AppendText(openFileDialog1.FileNames[i]);
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                for (int i = 0; i < openFileDialog1.FileNames.Length; i++)
                {
                    if (NightWallpapersRich.Lines.Length != 0)
                        NightWallpapersRich.AppendText("\n");
                    NightWallpapersRich.AppendText(openFileDialog1.FileNames[i]);
                }
            }
        }

    }
}
