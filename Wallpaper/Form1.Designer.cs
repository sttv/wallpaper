﻿namespace Wallpaper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.TrayMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Show = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.MorningWallpapersRich = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.HourOfMorning = new System.Windows.Forms.NumericUpDown();
            this.MinuteOfMorning = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CheckInterval = new System.Windows.Forms.CheckBox();
            this.CheckRandom = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.IntervalOfMorning = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.IntervalOfDay = new System.Windows.Forms.NumericUpDown();
            this.MinuteOfDay = new System.Windows.Forms.NumericUpDown();
            this.HourOfDay = new System.Windows.Forms.NumericUpDown();
            this.button5 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.DayWallpapersRich = new System.Windows.Forms.RichTextBox();
            this.IntervalOfEvening = new System.Windows.Forms.NumericUpDown();
            this.MinuteOfEvening = new System.Windows.Forms.NumericUpDown();
            this.HourOfEvening = new System.Windows.Forms.NumericUpDown();
            this.button7 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.EveningWallpapersRich = new System.Windows.Forms.RichTextBox();
            this.IntervalOfNight = new System.Windows.Forms.NumericUpDown();
            this.MinuteOfNight = new System.Windows.Forms.NumericUpDown();
            this.HourOfNight = new System.Windows.Forms.NumericUpDown();
            this.button8 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.NightWallpapersRich = new System.Windows.Forms.RichTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.CheckStartChange = new System.Windows.Forms.CheckBox();
            this.TrayMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HourOfMorning)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinuteOfMorning)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IntervalOfMorning)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IntervalOfDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinuteOfDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HourOfDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IntervalOfEvening)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinuteOfEvening)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HourOfEvening)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IntervalOfNight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinuteOfNight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HourOfNight)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(480, 54);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(43, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "WallpersChange";
            this.notifyIcon1.ContextMenuStrip = this.TrayMenu;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.Show_Click);
            // 
            // TrayMenu
            // 
            this.TrayMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Show,
            this.Exit});
            this.TrayMenu.Name = "TrayMenu";
            this.TrayMenu.Size = new System.Drawing.Size(104, 48);
            // 
            // Show
            // 
            this.Show.Name = "Show";
            this.Show.Size = new System.Drawing.Size(103, 22);
            this.Show.Text = "Show";
            this.Show.Click += new System.EventHandler(this.Show_Click);
            // 
            // Exit
            // 
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(103, 22);
            this.Exit.Text = "Exit";
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // MorningWallpapersRich
            // 
            this.MorningWallpapersRich.DetectUrls = false;
            this.MorningWallpapersRich.Location = new System.Drawing.Point(18, 35);
            this.MorningWallpapersRich.Name = "MorningWallpapersRich";
            this.MorningWallpapersRich.Size = new System.Drawing.Size(343, 79);
            this.MorningWallpapersRich.TabIndex = 2;
            this.MorningWallpapersRich.Text = "";
            this.MorningWallpapersRich.WordWrap = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(15, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Утро";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(367, 35);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(46, 79);
            this.button3.TabIndex = 5;
            this.button3.Text = "Add";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(480, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(93, 36);
            this.button4.TabIndex = 6;
            this.button4.Text = "Force Change wallpaper";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // HourOfMorning
            // 
            this.HourOfMorning.Location = new System.Drawing.Point(92, 9);
            this.HourOfMorning.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.HourOfMorning.Name = "HourOfMorning";
            this.HourOfMorning.Size = new System.Drawing.Size(40, 20);
            this.HourOfMorning.TabIndex = 10;
            this.HourOfMorning.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            // 
            // MinuteOfMorning
            // 
            this.MinuteOfMorning.Location = new System.Drawing.Point(161, 9);
            this.MinuteOfMorning.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.MinuteOfMorning.Name = "MinuteOfMorning";
            this.MinuteOfMorning.Size = new System.Drawing.Size(35, 20);
            this.MinuteOfMorning.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(74, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "h:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(141, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "m:";
            // 
            // CheckInterval
            // 
            this.CheckInterval.AutoSize = true;
            this.CheckInterval.BackColor = System.Drawing.Color.Transparent;
            this.CheckInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CheckInterval.Location = new System.Drawing.Point(419, 414);
            this.CheckInterval.Name = "CheckInterval";
            this.CheckInterval.Size = new System.Drawing.Size(94, 17);
            this.CheckInterval.TabIndex = 15;
            this.CheckInterval.Text = "Use interval";
            this.CheckInterval.UseVisualStyleBackColor = false;
            // 
            // CheckRandom
            // 
            this.CheckRandom.AutoSize = true;
            this.CheckRandom.BackColor = System.Drawing.Color.Transparent;
            this.CheckRandom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CheckRandom.Location = new System.Drawing.Point(419, 391);
            this.CheckRandom.Name = "CheckRandom";
            this.CheckRandom.Size = new System.Drawing.Size(93, 17);
            this.CheckRandom.TabIndex = 16;
            this.CheckRandom.Text = "Use random";
            this.CheckRandom.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(216, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Interval:";
            // 
            // timer1
            // 
            this.timer1.Interval = 60000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 60000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // IntervalOfMorning
            // 
            this.IntervalOfMorning.Location = new System.Drawing.Point(262, 9);
            this.IntervalOfMorning.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.IntervalOfMorning.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.IntervalOfMorning.Name = "IntervalOfMorning";
            this.IntervalOfMorning.Size = new System.Drawing.Size(50, 20);
            this.IntervalOfMorning.TabIndex = 19;
            this.IntervalOfMorning.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(318, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "minutes";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(529, 54);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(44, 23);
            this.button6.TabIndex = 21;
            this.button6.Text = "Stop";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // IntervalOfDay
            // 
            this.IntervalOfDay.Location = new System.Drawing.Point(262, 124);
            this.IntervalOfDay.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.IntervalOfDay.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.IntervalOfDay.Name = "IntervalOfDay";
            this.IntervalOfDay.Size = new System.Drawing.Size(50, 20);
            this.IntervalOfDay.TabIndex = 33;
            this.IntervalOfDay.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // MinuteOfDay
            // 
            this.MinuteOfDay.Location = new System.Drawing.Point(161, 124);
            this.MinuteOfDay.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.MinuteOfDay.Name = "MinuteOfDay";
            this.MinuteOfDay.Size = new System.Drawing.Size(35, 20);
            this.MinuteOfDay.TabIndex = 29;
            // 
            // HourOfDay
            // 
            this.HourOfDay.Location = new System.Drawing.Point(92, 124);
            this.HourOfDay.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.HourOfDay.Name = "HourOfDay";
            this.HourOfDay.Size = new System.Drawing.Size(40, 20);
            this.HourOfDay.TabIndex = 28;
            this.HourOfDay.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(367, 150);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(46, 79);
            this.button5.TabIndex = 27;
            this.button5.Text = "Add";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(15, 127);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "День";
            // 
            // DayWallpapersRich
            // 
            this.DayWallpapersRich.DetectUrls = false;
            this.DayWallpapersRich.Location = new System.Drawing.Point(18, 150);
            this.DayWallpapersRich.Name = "DayWallpapersRich";
            this.DayWallpapersRich.Size = new System.Drawing.Size(343, 79);
            this.DayWallpapersRich.TabIndex = 25;
            this.DayWallpapersRich.Text = "";
            this.DayWallpapersRich.WordWrap = false;
            // 
            // IntervalOfEvening
            // 
            this.IntervalOfEvening.Location = new System.Drawing.Point(262, 236);
            this.IntervalOfEvening.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.IntervalOfEvening.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.IntervalOfEvening.Name = "IntervalOfEvening";
            this.IntervalOfEvening.Size = new System.Drawing.Size(50, 20);
            this.IntervalOfEvening.TabIndex = 43;
            this.IntervalOfEvening.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // MinuteOfEvening
            // 
            this.MinuteOfEvening.Location = new System.Drawing.Point(161, 236);
            this.MinuteOfEvening.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.MinuteOfEvening.Name = "MinuteOfEvening";
            this.MinuteOfEvening.Size = new System.Drawing.Size(35, 20);
            this.MinuteOfEvening.TabIndex = 39;
            // 
            // HourOfEvening
            // 
            this.HourOfEvening.Location = new System.Drawing.Point(92, 236);
            this.HourOfEvening.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.HourOfEvening.Name = "HourOfEvening";
            this.HourOfEvening.Size = new System.Drawing.Size(40, 20);
            this.HourOfEvening.TabIndex = 38;
            this.HourOfEvening.Value = new decimal(new int[] {
            18,
            0,
            0,
            0});
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(367, 262);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(46, 79);
            this.button7.TabIndex = 37;
            this.button7.Text = "Add";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(15, 239);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(42, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "Вечер";
            // 
            // EveningWallpapersRich
            // 
            this.EveningWallpapersRich.DetectUrls = false;
            this.EveningWallpapersRich.Location = new System.Drawing.Point(18, 262);
            this.EveningWallpapersRich.Name = "EveningWallpapersRich";
            this.EveningWallpapersRich.Size = new System.Drawing.Size(343, 79);
            this.EveningWallpapersRich.TabIndex = 35;
            this.EveningWallpapersRich.Text = "";
            this.EveningWallpapersRich.WordWrap = false;
            // 
            // IntervalOfNight
            // 
            this.IntervalOfNight.Location = new System.Drawing.Point(262, 352);
            this.IntervalOfNight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.IntervalOfNight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.IntervalOfNight.Name = "IntervalOfNight";
            this.IntervalOfNight.Size = new System.Drawing.Size(50, 20);
            this.IntervalOfNight.TabIndex = 53;
            this.IntervalOfNight.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // MinuteOfNight
            // 
            this.MinuteOfNight.Location = new System.Drawing.Point(161, 352);
            this.MinuteOfNight.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.MinuteOfNight.Name = "MinuteOfNight";
            this.MinuteOfNight.Size = new System.Drawing.Size(35, 20);
            this.MinuteOfNight.TabIndex = 49;
            // 
            // HourOfNight
            // 
            this.HourOfNight.Location = new System.Drawing.Point(92, 352);
            this.HourOfNight.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.HourOfNight.Name = "HourOfNight";
            this.HourOfNight.Size = new System.Drawing.Size(40, 20);
            this.HourOfNight.TabIndex = 48;
            this.HourOfNight.Value = new decimal(new int[] {
            22,
            0,
            0,
            0});
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(367, 378);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(46, 79);
            this.button8.TabIndex = 47;
            this.button8.Text = "Add";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(15, 355);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(36, 13);
            this.label23.TabIndex = 46;
            this.label23.Text = "Ночь";
            // 
            // NightWallpapersRich
            // 
            this.NightWallpapersRich.DetectUrls = false;
            this.NightWallpapersRich.Location = new System.Drawing.Point(18, 378);
            this.NightWallpapersRich.Name = "NightWallpapersRich";
            this.NightWallpapersRich.Size = new System.Drawing.Size(343, 79);
            this.NightWallpapersRich.TabIndex = 45;
            this.NightWallpapersRich.Text = "";
            this.NightWallpapersRich.WordWrap = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(438, 336);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(85, 13);
            this.label24.TabIndex = 55;
            this.label24.Text = "_____________";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(419, 446);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 56;
            this.label6.Text = "-------";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(419, 433);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 57;
            this.label7.Text = "Current picture:";
            // 
            // CheckStartChange
            // 
            this.CheckStartChange.BackColor = System.Drawing.Color.Transparent;
            this.CheckStartChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CheckStartChange.Location = new System.Drawing.Point(419, 352);
            this.CheckStartChange.Name = "CheckStartChange";
            this.CheckStartChange.Size = new System.Drawing.Size(147, 33);
            this.CheckStartChange.TabIndex = 58;
            this.CheckStartChange.Text = "Change wallpaper when  program start";
            this.CheckStartChange.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 473);
            this.Controls.Add(this.CheckStartChange);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.IntervalOfNight);
            this.Controls.Add(this.MinuteOfNight);
            this.Controls.Add(this.HourOfNight);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.NightWallpapersRich);
            this.Controls.Add(this.IntervalOfEvening);
            this.Controls.Add(this.MinuteOfEvening);
            this.Controls.Add(this.HourOfEvening);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.EveningWallpapersRich);
            this.Controls.Add(this.IntervalOfDay);
            this.Controls.Add(this.MinuteOfDay);
            this.Controls.Add(this.HourOfDay);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.DayWallpapersRich);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.IntervalOfMorning);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CheckRandom);
            this.Controls.Add(this.CheckInterval);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.MinuteOfMorning);
            this.Controls.Add(this.HourOfMorning);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MorningWallpapersRich);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Сменщик обоев с блекджеком и шлюхами";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.TrayMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HourOfMorning)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinuteOfMorning)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IntervalOfMorning)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IntervalOfDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinuteOfDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HourOfDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IntervalOfEvening)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinuteOfEvening)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HourOfEvening)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IntervalOfNight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinuteOfNight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HourOfNight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip TrayMenu;
        private System.Windows.Forms.ToolStripMenuItem Show;
        private System.Windows.Forms.ToolStripMenuItem Exit;
        private System.Windows.Forms.RichTextBox MorningWallpapersRich;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.NumericUpDown HourOfMorning;
        private System.Windows.Forms.NumericUpDown MinuteOfMorning;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox CheckInterval;
        private System.Windows.Forms.CheckBox CheckRandom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.NumericUpDown IntervalOfMorning;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.NumericUpDown IntervalOfDay;
        private System.Windows.Forms.NumericUpDown MinuteOfDay;
        private System.Windows.Forms.NumericUpDown HourOfDay;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RichTextBox DayWallpapersRich;
        private System.Windows.Forms.NumericUpDown IntervalOfEvening;
        private System.Windows.Forms.NumericUpDown MinuteOfEvening;
        private System.Windows.Forms.NumericUpDown HourOfEvening;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RichTextBox EveningWallpapersRich;
        private System.Windows.Forms.NumericUpDown IntervalOfNight;
        private System.Windows.Forms.NumericUpDown MinuteOfNight;
        private System.Windows.Forms.NumericUpDown HourOfNight;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.RichTextBox NightWallpapersRich;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox CheckStartChange;
    }
}

